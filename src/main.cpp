#include <iostream>
#include <memory>
#include <vector>
#include <fstream>
#include <limits>

#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <fcntl.h>

// registers
constexpr uint16_t     R_R0 = 0;
constexpr uint16_t     R_R1 = 1;
constexpr uint16_t     R_R2 = 2;
constexpr uint16_t     R_R3 = 3;
constexpr uint16_t     R_R4 = 4;
constexpr uint16_t     R_R5 = 5;
constexpr uint16_t     R_R6 = 6;
constexpr uint16_t     R_R7 = 7;
constexpr uint16_t     R_PC = 8;    /* program counter */
constexpr uint16_t     R_COUNT = 9;


// OP codes
constexpr uint16_t     OP_HALT = 0;    /* stop execution and terminate the program */
constexpr uint16_t     OP_SET = 1;     /* set register <a> to the value of <b> */
constexpr uint16_t     OP_PUSH = 2;    /* push <a> onto the stack */
constexpr uint16_t     OP_POP = 3;     /* remove the top element from the stack and write it into <a>; empty stack = error */
constexpr uint16_t     OP_EQ = 4;      /* set <a> to 1 if <b> is equal to <c>; set it to 0 otherwise */
constexpr uint16_t     OP_GT = 5;      /* set <a> to 1 if <b> is greater than <c>; set it to 0 otherwise */
constexpr uint16_t     OP_JMP = 6;     /* jump to <a> */
constexpr uint16_t     OP_JT = 7;      /* if <a> is nonzero, jump to <b> */
constexpr uint16_t     OP_JF = 8;      /* if <a> is zero, jump to <b> */
constexpr uint16_t     OP_ADD = 9;     /* assign into <a> the sum of <b> and <c> (modulo 32768) */
constexpr uint16_t     OP_MULT = 10;    /* store into <a> the product of <b> and <c> (modulo 32768) */
constexpr uint16_t     OP_MOD = 11;     /* store into <a> the remainder of <b> divided by <c> */
constexpr uint16_t     OP_AND = 12;     /* stores into <a> the bitwise and of <b> and <c> */
constexpr uint16_t     OP_OR = 13;      /* stores into <a> the bitwise or of <b> and <c> */
constexpr uint16_t     OP_NOT = 14;     /* stores 15-bit bitwise inverse of <b> in <a> */
constexpr uint16_t     OP_RMEM = 15;    /* read memory at address <b> and write it to <a> */
constexpr uint16_t     OP_WMEM = 16;    /* write the value from <b> into memory at address <a> */
constexpr uint16_t     OP_CALL = 17;    /* write the address of the next instruction to the stack and jump to <a> */
constexpr uint16_t     OP_RET = 18;     /* remove the top element from the stack and jump to it; empty stack = halt */
constexpr uint16_t     OP_OUT = 19;     /* write the character represented by ascii code <a> to the terminal */
constexpr uint16_t     OP_IN = 20;       /* read a character from the terminal and write its ascii code to <a>; 
                it can be assumed that once input starts, it will continue until a newline is encountered;
                this means that you can safely read whole lines from the keyboard and trust that they will be fully read */
constexpr uint16_t     OP_NOOP = 21;

 
std::vector<uint8_t> memory(1 << 16);
std::vector<uint16_t> stack;
std::vector<uint16_t> registers(R_COUNT);

uint16_t mem_read(uint16_t addr){  
    //      10101010          00110011     ->     0011001100000000 -> 0011001110101010
    return memory[addr] | (memory[addr + 1] << 8);
}

uint16_t nextWord(){
    uint16_t temp = mem_read(registers[R_PC]);
    registers[R_PC] += 2;
    return temp;
}

bool is_valid(uint16_t &op){
    if(op < 32768){
        return true;
    }else if(op < 32776){
        op = registers[op - 32768];
        return true;
    }else{
        std::cout << "\n INVALID OP \n";
        return false;
    }    
}

bool is_reg(uint16_t op){
    if(op > 32768 && op < 32776){
        return true;
    }else{
        return false;
    }
}

int main(int argc, const char* argv[]){

    std::ifstream file("challenge.bin", std::ios::binary | std::ios::ate);
    file.ignore( std::numeric_limits<std::streamsize>::max() );
    file.seekg (0, std::ios::end);
    std::streamsize fileSize = file.tellg();
    file.seekg(0, std::ios::beg);
    std::cout << "File size: " << fileSize << "\n";

    std::vector<char> buffer(fileSize);
    if (file.read(buffer.data(), fileSize))
    {
        std::cout << "Buffer size: " << buffer.size() << "\n";
        for(int i = 0; i < buffer.size(); ++i){
            memory[i] = buffer[i];
        }
    }


    constexpr uint16_t PC_START = 0;
    registers[R_PC] = PC_START;

    bool running = true;
    while (running)
    {
        /* FETCH */
        uint16_t op = nextWord();

        switch (op)
        {
            case OP_HALT:
                {
                    std::cout << "HALT\n";
                    running = false;
                }
                break;
            case OP_SET:
                {
                    std::cout << "SET\n";
                    uint16_t op1,op2;
                    op1 = nextWord();
                    op2 = nextWord();
                
                    if(is_valid(op2)){
                        if(op1 < 32768){
                            std::cout << "Wrong value\n";
                        }else if(op1 < 32776){
                            registers[op1 - 32776] = op2;
                        }else{
                            std::cout << "Error\n";
                        }
                    }
                }
                break;
            case OP_PUSH:
                {
                    std::cout << "PUSH\n";
                    uint16_t op1;
                    op1 = nextWord();
                    if(is_valid(op1)){
                        stack.push_back(op1);
                    }
                }
                break;
            case OP_POP:
                {
                    std::cout << "POP\n";
                    uint16_t op1;
                    op1 = nextWord();
                    if(stack.empty()){
                        break;
                    }
                    if(op1 < 32768){
                          std::cout << "Error\n";
                    }else if(op1 < 32776){
                        registers[op1 - 32776] = stack.at(stack.size() -1);
                        stack.pop_back(); 
                    }else{
                        std::cout << "Error\n";
                    }

                }
                break;
            case OP_EQ:
                {
                    uint16_t op1,op2,op3;
                    op1 = nextWord();
                    op2 = nextWord();
                    op3 = nextWord();
                    if(op1 > 32768 && op1 < 32776){
                        if(is_reg(op2) && is_reg(op3)){
                            if(registers[op2 - 32768] == registers[op3 - 32768]){
                                registers[op1] = 1;
                            }else{
                                registers[op1] = 0;
                            }
                        }else{
                            std::cout << "\n<br> or <c> are not valid.\n";
                        }
                    }else{
                        std::cout << "\n<a> is not valid.\n";
                    }
                }
                break;
            case OP_GT:
                {
                    std::cout << "GT\n";
                    uint16_t op1,op2,op3;
                    op1 = nextWord();
                    op2 = nextWord();
                    op3 = nextWord();
                    if(op1 > 32768 && op1 < 32776){
                        if(is_reg(op2) && is_reg(op3)){
                            if(registers[op2 - 32768] > registers[op3 - 32768]){
                                registers[op1] = 1;
                            }else{
                                registers[op1] = 0;
                            }
                        }else{
                            std::cout << "\n<br> or <c> are not valid.\n";
                        }
                    }else{
                        std::cout << "\n<a> is not valid.\n";
                    }
                }
                break;
            case OP_JMP:
                {
                    std::cout << "JMP\n";  
                    uint16_t op1;
                    op1 = nextWord();   
                    if(is_reg(op1)){
                        registers[R_PC] = op1 - 32768;
                    }else{
                        std::cout << "\nWrong jump op: " << op1 << "\n";
                    }
                    registers[R_PC] = op1/2;  
                }
                break;
            case OP_JT:
                {
                    std::cout << "JT\n";
                    uint16_t op1,op2;
                    op1 = nextWord();
                    op2 = nextWord();
                    if(is_reg(op1)){
                        if(registers[op1 - 32768] != 0){
                            if(is_valid(op2)){
                                registers[R_PC] = op2;
                            }else{
                                std::cout << "JT not valid op2\n";
                            }
                        }
                    }
                    
                }
                break;
            case OP_JF:
                {
                    std::cout << "JF\n";
                    uint16_t op1,op2;
                    op1 = nextWord();
                    op2 = nextWord();
                    if(is_reg(op1)){
                        if(registers[op1 - 32768] == 0){
                            if(is_valid(op2)){
                                registers[R_PC] = op2;
                            }else{
                                std::cout << "JT not valid op2\n";
                            }
                        }
                    }
                }
                break;
            case OP_ADD:
                {
                    std::cout << "ADD\n"; 
                    uint16_t op1,op2,op3;
                    op1 = nextWord();
                    op2 = nextWord();     
                    op3 = nextWord();    
                    if(is_reg(op1) && is_reg(op2) && is_reg(op3)){
                        registers[op1] = (registers[op2] + registers[op3]) % 32768
                    }
                           
                }
                break;
            case OP_MULT:
                {
                    std::cout << "MULT\n";
                    uint16_t op1,op2,op3;
                    op1 = nextWord();
                    op2 = nextWord();     
                    op3 = nextWord();   
                }
                break;
            case OP_MOD:
                {
                    std::cout << "MOD\n";
                    uint16_t op1,op2,op3;
                    op1 = nextWord();
                    op2 = nextWord();     
                    op3 = nextWord();  
                }
                break;
            case OP_AND:
                {
                    std::cout << "AND\n";    
                    uint16_t op1,op2,op3;
                    op1 = nextWord();
                    op2 = nextWord();     
                    op3 = nextWord();                
                }
                break;
            case OP_OR:
                {
                    std::cout << "OR\n";
                    uint16_t op1,op2,op3;
                    op1 = nextWord();
                    op2 = nextWord();     
                    op3 = nextWord();  
                }
                break;
            case OP_NOT:
                {
                    std::cout << "NOT\n";
                    uint16_t op1,op2,op3;
                    op1 = nextWord();
                    op2 = nextWord();     
                    op3 = nextWord();  
                }
                break;
            case OP_RMEM:
                {
                    std::cout << "RMEM\n";
                    uint16_t op1,op2;
                    op1 = nextWord();
                    op2 = nextWord();     

                }
                break;
            case OP_WMEM:
                {
                    std::cout << "WMEM\n";
                    uint16_t op1,op2;
                    op1 = nextWord();
                    op2 = nextWord();  
                }
                break;
            case OP_CALL:
                {
                    std::cout << "CALL\n";
                    uint16_t op1;
                    op1 = nextWord();
                }
                break;
            case OP_RET:
                {
                    std::cout << "RET\n";
                }
                break;
            case OP_OUT:
                {
                    uint16_t op1;
                    op1 = nextWord();
                    if(op1 < 32768){
                        std::cout << (char)op1;
                    }else if(op1 < 32776){
                        std::cout << registers[op1 - 32768];
                    }else{
                        std::cout << "\n INVALID OP \n";
                    }
                }
                break;
            case OP_IN:
                {
                    std::cout << "IN\n";
                    uint16_t op1;
                    op1 = nextWord();
                } 
                break;
            case OP_NOOP:
                {
                }
                break;
            defaut:
                std::cout << "Wrong OP_CODE...\n";
                abort();
                break;
        }
    }
  
    return 0;
}

